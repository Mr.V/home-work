const inputs = document.getElementById('services');

inputs.onclick = function(event) {
    const name = event.target.className;
    const classCollection = document.getElementsByClassName(name);
    const activeText = document.getElementsByClassName('active')[0];

    if (classCollection[1]) {
        if (!activeText) {
            classCollection[1].classList.add('active');
        } else {
            activeText.classList.remove('active');
            classCollection[1].classList.add('active');
        }
    }

    const ev = event.target;
    const activeInput = document.getElementsByClassName('color')[0];
    const triangleVisible = document.getElementsByClassName('triangle_visible')[0];

    if (!activeInput) {
        ev.classList.add('color');
        ev.nextElementSibling.classList.add('triangle_visible')
    } else {
        activeInput.classList.remove('color');
        triangleVisible.classList.remove('triangle_visible');
        ev.classList.add('color');
        ev.nextElementSibling.classList.add('triangle_visible');
    }
};

$(function() {
    $('#amazing_menu div').click(function() {
        $('#amazing_menu div').css({ 'color': '#4e4e4e', 'border-top': '1px #ebebeb solid' });
        $(this).css({ 'color': '#18cfab', 'border-top': '2px #18cfab solid' });

        let ind = $(this).attr('class');
        $('.amazing_pictures img').hide();
        let x = document.getElementsByClassName(`${ind}`);
        for (let i = 1; i <= x.length; i++) {
            x[i].style.display = "block";
        }
    })
});