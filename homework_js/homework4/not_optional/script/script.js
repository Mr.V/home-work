function createUser() {
    let firstName = prompt('Input your first name');
    let lastName = prompt('Input your last name');

    let newUser = {};
    newUser.firstName = firstName;
    newUser.lastName = lastName;

    newUser.getLogin = function() {
        let userName = this.firstName.charAt(0) + this.lastName;
        return userName.toLowerCase();
    };

    return newUser;
}

let user = createUser();
console.log(user.getLogin());