function createUser() {
    let firstName = prompt('Input your first name');
    let lastName = prompt('Input your last name');

    let newUser = {};
    newUser.firstName = firstName;
    newUser.lastName = lastName;

    newUser.getLogin = function() {
        console.log(this);
        let userName = firstName.charAt(0) + lastName;
        return userName.toLowerCase();
    };

    Object.defineProperty(newUser, 'firstName', {
        configurable: true,
        writable: false
    })

    Object.defineProperty(newUser, 'lastName', {
        configurable: true,
        writable: false
    })

    return newUser;
}

const user = createUser();