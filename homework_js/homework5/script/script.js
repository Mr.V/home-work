function createUser() {
    const firstName = prompt('Input your first name');
    const lastName = prompt('Input your last name');
    let birthday = prompt('Input your birthday');
    birthday = birthday.split('.');
    const date = new Date(birthday[2], birthday[1] - 1, birthday[0]);
    console.log(date)

    const newUser = {};
    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.birthday = birthday;
    newUser.birthDate = date;

    newUser.getLogin = function() {
        console.log(this);
        const userName = firstName.charAt(0) + lastName;
        return userName.toLowerCase();
    };

    newUser.getAge = function() {
        console.log(this);
        const today = new Date();
        const age = today.getFullYear() - this.birthDate.getFullYear();
        const m = today.getMonth() - this.birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    };

    newUser.getPassword = function() {
        console.log(this);
        const password = this.firstName.charAt(0).toUpperCase() + this.lastName.toLowerCase() + this.birthDate.getFullYear();
        return password;
    };

    Object.defineProperty(newUser, 'firstName', {
        configurable: true,
        writable: false
    })

    Object.defineProperty(newUser, 'lastName', {
        configurable: true,
        writable: false
    })



    return newUser;
}

const user = createUser();