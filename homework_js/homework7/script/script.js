const ul = document.createElement('ul');
document.body.appendChild(ul);
const div = document.createElement('div');

newArr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', [44, 55], 'Lviv'];

liInArray(newArr, ul);
showNumbersAndRemoveAll();

function liInArray(arr, parent) {
    arr.map(function(value) {
        if (Array.isArray(value)) {
            const newUl = document.createElement('ul');
            ul.appendChild(newUl);
            liInArray(value, newUl)
        } else {
            const li = document.createElement('li');
            parent.appendChild(li);
            li.innerHTML = `${value}`;
        }
    })
}

function showNumbersAndRemoveAll() {
    let i = 10;
    let timerId = setInterval(function() {
        div.innerHTML = `${i}`;
        document.body.appendChild(div);
        if (i === 0) clearInterval(timerId);
        i--;
        setTimeout(function() {
            document.body.removeChild(div);
            document.body.removeChild(ul);
        }, 10000)
    }, 1000);
}