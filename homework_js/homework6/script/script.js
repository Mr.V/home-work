const basicArray = ['Apple', 23, null, 'Orange', 45, 'Blue', 50];

function filterBy(arr, filterType) {
    let result = arr.filter(function(el) {
        return typeof el !== filterType;
    });

    return result;
}

const array = filterBy(basicArray, 'string');